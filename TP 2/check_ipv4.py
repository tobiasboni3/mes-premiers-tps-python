#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 09:00:20 2019
@author: b19018426
"""


def check_ipv4(x):
    """
    Teste si la liste représente une IP valide
    """
    validite = None
    if type(x) == list:
        if len(x) == 4:
            for i in range(len(x)):
                if type(x[i]) == int and ((int(x[i]) < 256) and int((x[i]) >= 0)):
                    validite = True
                else:
                    validite = None
                    return validite
        if validite:
            validite = x
        return validite


def main():
    
    ipv4 = [124, 33, 113, 5]
    valeur1 = check_ipv4(ipv4)
    assert valeur1 == ipv4
    print("1er Exemple valide")
    
    ip2 = []
    valeur2 = check_ipv4(ip2)
    assert valeur2 is None
    print("2ème Exemple valide")
    
    ip3 = [[2, 3], 4, 5, 6]
    valeur3 = check_ipv4(ip3)
    assert valeur3 is None
    print("3ème Exemple valide")
    
    ip4 = [1, 288, 0, 0]
    valeur4 = check_ipv4(ip4)
    assert valeur4 is None
    print("4ème Exemple valide")
    
    ip5 = [0, "BOUM", 1, 2]
    valeur5 = check_ipv4(ip5)
    assert valeur5 is None
    print("5ème Exemple valide")
    

if __name__ == "__main__":
    main()

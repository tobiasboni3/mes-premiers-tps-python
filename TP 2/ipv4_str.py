#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 14:56:29 2019
@author: b19018426
"""


def ipv4_vers_chaine(x):
    """
    Transforme une liste en chaine de caractère représentant une IP
    """
    y = str()
    if x == None:
        return None
    if type(x) == list:

        for i in range(len(x)):
            y += str(x[i])
            if i != len(x) - 1:
                y += "."
    return y


def main():
    
    ipv4 = [124, 33, 113, 5]
    valeur = ipv4_vers_chaine(ipv4)
    assert valeur == "124.33.113.5"

    ip2 = []
    v2 = ipv4_vers_chaine(ip2)
    assert v2 == ""

    print("test réussi")


if __name__ == "__main__":
    main()

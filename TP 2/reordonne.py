#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 17 14:04:16 2019
@author: b19018426
"""


def reordonne(l):
    """
    Soit une liste sous la forme [valeur, index [...]]
    On déplace la valeur sur l'index indiqué
    """
    if type(l) != list or len(l) % 2:
        return None
    new_list = [0] * int(len(l) / 2)
    for i in range(0, len(l), 2):
        if (l[i + 1]) > len(l) // 2:
            return None
        new_list[(l[i + 1] - 1)] = l[i]
    return new_list


def main():
    li1 = [10, 3, 12, 4, 36, 1, 44, 2]
    l1 = reordonne(li1)
    assert l1 == [36, 44, 10, 12]

    li2 = ["boum", 3, 12, 4, 36, 1, 44, 2]
    l2 = reordonne(li2)
    assert l2 == [36, 44, "boum", 12]

    li3 = False
    l3 = reordonne(li3)
    assert l3 == None

    li4 = [10, 3, 12, 4, 36]
    l4 = reordonne(li4)
    assert l4 == None

    li5 = [10, 7, 12, 4]
    l5 = reordonne(li5)
    assert l5 == None

    print("Test OK")


if __name__ == "__main__":
    main()

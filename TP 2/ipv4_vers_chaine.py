#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 09:00:20 2019
@author: b19018426
"""

import ipv4_str
import check_ipv4


def main():
    """
    Renvoi une chaine de caractère contenant l'ip si elle est valide
    sinon renvoi None
    """
    ip1 = [124, 33, 113, 5]
    ip2 = []
    ip3 = "BOUM"
    ip4 = [1, 288, 0, 0]
    ip5 = [1, "boum", 4, 5]

    v1 = check_ipv4.check_ipv4(ip1)
    assert v1 == ip1
    res1 = ipv4_str.ipv4_vers_chaine(v1)
    print("1er IP :", res1)

    v2 = check_ipv4.check_ipv4(ip2)
    assert v2 == None
    res2 = ipv4_str.ipv4_vers_chaine(v2)
    print("2ème IP :", res2)

    v3 = check_ipv4.check_ipv4(ip3)
    assert v3 == None
    res3 = ipv4_str.ipv4_vers_chaine(v3)
    print("3ème IP :", res3)

    v4 = check_ipv4.check_ipv4(ip4)
    assert v4 == None
    res4 = ipv4_str.ipv4_vers_chaine(v4)
    print("4ème IP :", res4)

    v5 = check_ipv4.check_ipv4(ip5)
    assert v5 == None
    res5 = ipv4_str.ipv4_vers_chaine(v5)
    print("5ème IP :", res5)


if __name__ == "__main__":
    main()

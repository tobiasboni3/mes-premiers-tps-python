#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 17 14:04:16 2019
@author: b19018426
"""
import ipv4_str
import check_ipv4


def cherche_domaine(dom, lst):
    """
    Pioche l'IP correspondant au domaine
    """
    if type(dom) != str or type(lst) != list or len(lst) != 2 or len(lst[0]) != len(lst[1]):
        return None
    for i in range(1):
        for j in range(len(lst[i])):
            if lst[0][j] == dom:
                iplist = lst[1][j]
                if check_ipv4.check_ipv4(iplist) is not None:
                    ip = ipv4_str.ipv4_vers_chaine(iplist)
                    return ip
    return None


def main():
    """
    Tests unitaires
    """
    lst = [["univ-amu.fr", "enseignementsup-recherche.gouv.fr", "seenthis.net"],
           [[139, 124, 244, 38], [185, 75, 143, 29], [217, 182, 178, 243]]]

    dom1 = "univ-amu.fr"
    d1 = cherche_domaine(dom1, lst)
    assert d1 == "139.124.244.38"
    print("Cas normal d'utilisation 1 [OK]")

    dom2 = "seenthis.net"
    d2 = cherche_domaine(dom2, lst)
    assert d2 == "217.182.178.243"
    print("Cas normal d'utilisation 2 [OK]")

    dom3 = "boum.ru"
    d3 = cherche_domaine(dom3, lst)
    assert d3 == None
    print("Mauvais domaine [OK]")

    lst2 = [["univ-amu.fr", "enseignementsup-recherche.gouv.fr", "seenthis.net"],
           [[139, 124, 244, 38], [185, 75, 143, 29], [217, 182, 178]]]
    d4 = cherche_domaine(dom2, lst2)
    assert d4 == None
    print("Mauvaise ip dans la liste entrée [OK]")

    lst3 = [["univ-amu.fr", "enseignementsup-recherche.gouv.fr", "seenthis.net"],
            [[139, 124, 244, 38], [185, 75, 143, 29]]]
    d5 = cherche_domaine(dom1, lst3)
    assert d5 == None
    print("Liste incomplète [OK]")

    lst4 = [["univ-amu.fr", "enseignementsup-recherche.gouv.fr", "seenthis.net"],
            [[139, 124, 244, 38], [185, 75, 143, 29], "BOUM"]]
    d6 = cherche_domaine(dom2, lst4)
    assert d6 == None
    print("Mauvais type dans la liste [OK]")


if __name__ == "__main__":
    main()

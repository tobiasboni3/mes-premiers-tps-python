#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 11:12:43 2020
@author: b19018426
"""


import sys
import affiche


def main():

    if sys.argv[0]:
        for i in range(1,len(sys.argv)):
            affiche.cat(sys.argv[i])


if __name__ == "__main__":
    main()

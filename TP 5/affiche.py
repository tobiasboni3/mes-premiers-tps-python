#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 10:39:42 2020
@author: b19018426
"""

import os


def cat(chemin):
    """
    Affiche le contenu d'un fichier texte ligne par ligne
    """
    if type(chemin) is not str:
        return
    if not os.path.isfile(chemin):
        content = "Fichier inexistant : " + chemin
        print(content)
        return 
    with open(chemin,"r", encoding="UTF-8") as fd:
        l = fd.readline()
        while l != "":
            l = l.rstrip('\n')
            print(l)
            l = fd.readline()
        
    
def main():
    
    chemin = input("Entrez un chemin : ")
    cat(chemin)
    
    #Test unitaire
    #cat("/amuhome/b19018426/src/Python/M1207/fichier.txt")
    #cat("/amuhome/b19018426/src/Python/M1207/fichier.boum")

if __name__ == "__main__":
    main()

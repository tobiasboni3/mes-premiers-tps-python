#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 11:58:53 2020
@author: b19018426
"""


import sys


def resoud(hote):
    """
    Donne l'ip correspondante depuis /etc/hosts
    """
    with open("/etc/hosts","r", encoding="UTF8") as fd:
        content = fd.read()
        lignes = content.rstrip('\n').split('\n')
        for no in range(len(lignes)):
            if hote in lignes[no]:
                LAligne = lignes[no].split()
                print(LAligne[0])
            else:
                return ""
    return ""


def main():

    if sys.argv[1]:
        hote = sys.argv[1]
        print(resoud(hote))


if __name__ == "__main__":
    main()

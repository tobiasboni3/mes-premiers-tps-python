#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 14:27:07 2019
@author: b19018426
"""


def min_max(note):
    global note_mini, note_maxi
    if note_mini == -1 or note_maxi == -1:
        note_mini = note
        note_maxi = note
    elif note < note_mini:
        note_mini = note
    elif note > note_maxi:
        note_maxi = note


def saisie_intervalle(mini, maxi):
    note = -1
    while note < mini or note > maxi:
        note = int(input("Saisir note : "))
    return note


def main():

    global note_mini
    global note_maxi
    note_mini = int()
    note_maxi = int()
    sommeDeNotes = int()
    i = 0
    mini, maxi = 0, 20
    try:
        while True:
            v = saisie_intervalle(mini, maxi)
            if mini <= v <= maxi:
                sommeDeNotes += v
                min_max(v)
                i += 1
    except ValueError:
        pass

    moyenne = sommeDeNotes / i

    print("Moyenne : ", moyenne)
    print("Note mini : ", note_mini, "  Note maxi : ", note_maxi)


if __name__ == '__main__':
    main()

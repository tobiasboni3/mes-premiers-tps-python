#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 13:36:02 2019
@author: b19018426
"""

nombre1 = int(input("Saisir le premier nombre :"))
nombre2 = int(input("Saisir le deuxième nombre :"))

somme = nombre1 + nombre2
print("La somme de ", nombre1, " et ", nombre2, " est ", somme)

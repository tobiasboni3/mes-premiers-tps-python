#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 14:08:29 2019
@author: b19018426
"""
note, i, mini, maxi, sommeDeNotes = 0, -1, -1, -1, 0

while 20 >= note >= 0:
    note = int(input("Saisir une note : "))

    if 20 >= note >= 0:
        sommeDeNotes += note

        if mini == -1 or maxi == -1:
            mini = note
            maxi = note

        if note < mini:
            mini = note

        if note > maxi:
            maxi = note

    i += 1

moyenne = sommeDeNotes / i
print("Moyenne : ", moyenne)
print("mini : ", mini, "  maxi : ", maxi)

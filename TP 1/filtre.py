#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 13:45:42 2019
@author: b19018426
"""

entier = -1

while entier > 10 or entier < 0:
    entier = int(input("Saisir un nombre entier entre 0 et 10 : "))
print("Vous avez saisi un nombre entre 0 et 10 : ", entier)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 14:56:29 2019
@author: b19018426
"""


def saisie_intervalle(x=0, y=20):
    note = x - 1
    while note <= x or note >= y:
        note = int(input("Saisir note : "))
    return note


x = int(input("Intervalle min : "))
y = int(input("Intervalle max : "))

if x < y:
    val = saisie_intervalle(x, y)
else:
    x, y = y, x
    val = saisie_intervalle(x, y)
print(val)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 13:39:02 2019
@author: b19018426
"""
import math

nombre = float(input("Saisir un nombre flottant : "))

if nombre >= 0:
    carre = math.sqrt(nombre)
    print("La racine carré de ", nombre, " est ", carre)
else:
    print("Erreur")

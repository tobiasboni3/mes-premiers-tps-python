#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 15:41:18 2019
@author: b19018426
"""
import check_ipv4

def genere_masque(cidr):
    """
    Transforme un nombre en masque
    """
    if type(cidr) != int or cidr > 32 or cidr <= 0 or cidr is None:
        return

    result = ""
    i = 0
    for y in range(0, cidr):
        if i % 8 == 0 and i != 0:
            result += "."
        result += "1"
        i += 1
    for y in range(0, 33 - len(result), 1):
        if i % 8 == 0 and i != 0:
            result += "."
        result += "0"
        i += 1
    result = result.split(".")
    for i in range(0, 4, 1):
        result[i] = int(str(int(result[i])), 2)
    return result


def main():
    masque1 = 19
    test1 = genere_masque(masque1)
    print(test1)

    masque2 = "boum"
    test2 = genere_masque(masque2)
    print(test2)
    
    masque3 = 32
    test3 = genere_masque(masque3)
    print(test3)


if __name__ == "__main__":
    main()

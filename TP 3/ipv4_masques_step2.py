#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 15:41:18 2019
@author: b19018426
"""
import check_ipv4


def genere_masque(cidr):
    """
    Transforme un nombre CDIR en masque
    """
    if type(cidr) != int or cidr > 32 or cidr <= 0 or cidr is None:
        return
    result = ""
    i = 0
    for y in range(0, cidr):
        if i % 8 == 0 and i != 0:
            result += "."
        result += "1"
        i += 1
    for y in range(0, 33 - len(result), 1):
        if i % 8 == 0 and i != 0:
            result += "."
        result += "0"
        i += 1
    result = result.split(".")
    for i in range(0, 4, 1):
        result[i] = int(str(int(result[i])), 2)
    return result


def sous_reseau(adr, cidr):
    if check_ipv4.check_ipv4(adr) == None:
        return
    sousres = list()
    for i in range(4):
        sousres.append(adr[i] & cidr[i])
    return sousres


def main():
    masque1 = 19
    test1 = genere_masque(masque1)
    assert test1 == [255, 255, 224, 0]
    print("Masque de 19 est bien égale à 255,255,224,0 [OK]")

    masque2 = "boum"
    test2 = genere_masque(masque2)
    assert test2 == None
    print("Une autre type renvoi None [OK]")

    masque3 = 32
    test3 = genere_masque(masque3)
    assert test3 == [255, 255, 255, 255]
    print("Masque de 32 renvoi bien 255,255,255,255 [OK]")

    masque4 = 33
    test4 = genere_masque(masque4)
    assert test4 == None
    print("Un mauvais masque renvoi None [OK]")

    adr1 = [192, 168, 1, 1]
    masque4 = 16
    cidr1 = genere_masque(masque4)
    sr1 = sous_reseau(adr1, cidr1)
    assert sr1 == [192, 168, 0, 0]
    print("Le sous réseau de 192,168,1,1 avec un masque de 16 est bien 192,168,0,0 [OK]")

    adr2 = [256, 168, 1, 1]
    sr2 = sous_reseau(adr2, cidr1)
    assert sr2 == None
    print("Le sous réseau d'une mauvaise adresse avec un masque de 16 renvoi None [OK]")

    adr3 = 192
    sr3 = sous_reseau(adr3, cidr1)
    assert sr3 == None
    print("Le sous réseau d'un mauvais type avec un masque de 16 renvoi None [OK]")


if __name__ == "__main__":
    main()

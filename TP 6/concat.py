#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 10 10:23:06 2020
@author: b19018426
"""

import sys
import os


def verifie_fichier(chemin):
    if not os.path.exists(chemin):
        print("Le fichier ", chemin, " est inexistant.")
        return False
    elif os.path.isfile(chemin):
        print("Le fichier ", chemin, " est un fichier.")
        return True
    elif os.path.isdir(chemin):
        print("Le fichier ", chemin, " est un repertoire.")
        return False
    else:
        print("Erreur")
        return 


def copie(fd_lect, fd_ecr):
    """
    Copie le contenu du fichier fd_lect, et l'écrit dans fd_ecr
    """
    content = fd_lect.readline()
    while content != "":
        fd_ecr.write(content)
        content = fd_lect.readline()
    fd_ecr.write("\n")
    return fd_ecr


def main():
    if len(sys.argv) != 4:
        return
    chemin1 = sys.argv[1]
    chemin2 = sys.argv[2]
    chemin3 = sys.argv[3]
    if verifie_fichier(chemin1) and verifie_fichier(chemin2):
        with open(chemin1,"r", encoding="utf-8") as fd_lect1, open(chemin2,"r", encoding="utf-8") as fd_lect2, open(chemin3, "w", encoding="utf-8") as fd_ecr :
            copie(fd_lect1, fd_ecr)
            copie(fd_lect2, fd_ecr)
    else:
        print("Mauvais chemin(s)")
        return  
        
        

if __name__ == "__main__":
    main()


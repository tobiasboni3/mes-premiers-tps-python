#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 10 10:05:20 2020
@author: b19018426
"""


def main():
    with open("tables.txt", "w") as fd:
        for tbl in range(2,10,1):
            for elt in range(1,10,1):
                ca = tbl * elt
                li = str(tbl) + " * " + str(elt) + " = " + str(ca) + '\n'
                fd.write(str(li))
            fd.write("\n")
        

if __name__ == "__main__":
    main()


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 14:56:29 2019
@author: b19018426
"""


def ipv4_vers_chaine(x):
    """
    Transforme une liste en chaine de caractère représentant une IP
    """
    y = ""
    if x is None:
        return None
    if type(x) == list:
        for i in range(len(x)):
            y += str(x[i])
            if i != len(x)-1:
                y += "."
    return y


def chaine_vers_ipv4(x):
    """
    Transforme une chaine de caractère en liste
    """
    if type(x) is not str:
        return None
    y = x.split(".")
    if len(y) != 4:
        return None
    for i in range(len(y)):
        if not y[i].isdigit():
            return None
        y[i] = int(y[i])
        if y[i] > 256 or y[i] <= 0:
            return None
    return y


def main():
    val1 = chaine_vers_ipv4("192.168.1.1")
    assert val1 == [192, 168, 1, 1]
    
    val2 = chaine_vers_ipv4("192.300.1.1")
    assert val2 == None
    
    val3 = chaine_vers_ipv4("192.1.1")
    assert val3 == None
    
    val4 = chaine_vers_ipv4("192.1.boom.1")
    assert val4 == None
    
    print("ok")

    ipv4 = [124, 33, 113, 5]
    valeur = ipv4_vers_chaine(ipv4)
    assert valeur == "124.33.113.5"
    
    ip2 = []
    v2 = ipv4_vers_chaine(ip2)
    assert v2 == ""

    print("test réussi")


if __name__ == "__main__":
    main()

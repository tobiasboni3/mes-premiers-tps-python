#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 20 10:09:26 2019
@author: b19018426
"""

import string
import chaine


def chiffre_cesar(txt, cle):
    """
    Remplace chaque lettre d'une message par une 
    lettre à distance fixe dans l'ordre de l'aphabet.
    """
    if txt is None:
        return None

    if type(cle) != int:
        return None

    result = ""
    alphabet = string.ascii_lowercase

    for i in range(len(txt)):
        if txt[i] == " ":
            result += txt[i]
        pos = alphabet.find(txt[i])
        posi = (pos + cle) % 26
        result += alphabet[posi]
    return result


def main():
    cle = 3
    txt = chaine.nettoie("azoro")
    print(chiffre_cesar("azoro", cle))
    #    assert (chiffre_cesar(txt,cle)) == "defghi"

    cle = 40
    txt = chaine.nettoie("abcdef")
    assert (chiffre_cesar(txt, cle)) == "opqrst"

    print("ok")


if __name__ == "__main__":
    main()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 20 10:09:26 2019
@author: b19018426
"""

import string
import chaine


def chiffre_cesar(txt, cle):
    """
    Remplace chaque lettre d'une message par une
    lettre à distance fixe dans l'ordre de l'aphabet.
    """
    if txt is None:
        return None
    if type(cle) != int:
        return None
    result = ""
    alphabet = string.ascii_lowercase
    txt = chaine.nettoie(txt)
    for i in range(len(txt)):
        if txt[i] == " ":
            result += txt[i]
        else:
            pos = alphabet.find(txt[i])
            posi = (pos + cle) % 26
            result += alphabet[posi]
    return result


def dechiffre_cesar(txt, cle):
    """
    Décale dans l'alphabet pour obtenir le message de départ
    """
    if txt is None:
        return None
    if type(cle) != int:
        return None
    result = ""
    alphabet = string.ascii_lowercase
    txt = chaine.nettoie(txt)
    for i in range(len(txt)):
        if txt[i] == " ":
            result += txt[i]
        else:
            pos = alphabet.find(txt[i])
            posi = (pos - cle) % 26
            result += alphabet[posi]
    return result


def chiffre_vigenere(msg, cle):
    """
    Chiffrement avec le chiffre de Vigenère
    """
    if msg is None:
        return None
    if type(cle) != str:
        return None
    cle = cle.replace(" ", "")
    indcle = 0
    result = ""
    alphabet = string.ascii_lowercase
    msg = chaine.nettoie(msg)
    for i in range(len(msg)):
        if msg[i] == " ":
            result += msg[i]
            indcle -= 1
        else:
            pos = alphabet.find(msg[i])
            if indcle >= len(cle):
                indcle = 0
            posCle = alphabet.find(cle[indcle])
            posi = (pos + posCle) % 26
            result += alphabet[posi]
            indcle += 1
    return result


def dechiffre_vigenere(msg, cle):
    """
    Chiffrement avec le chiffre de Vigenère
    """
    if msg is None:
        return None
    if type(cle) != str:
        return None
    cle = cle.replace(" ", "")
    indcle = 0
    result = ""
    alphabet = string.ascii_lowercase
    msg = chaine.nettoie(msg)
    for i in range(len(msg)):
        if msg[i] == " ":
            result += msg[i]
            indcle -= 1
        else:
            pos = alphabet.find(msg[i])
            if indcle >= len(cle):
                indcle = 0
            posCle = alphabet.find(cle[indcle])
            posi = (pos - posCle) % 26
            result += alphabet[posi]
            indcle += 1
    return result


def main():
    # Test unitaire chiffrage
    cle1 = 3
    assert chiffre_cesar("azoro", cle1) == "dcrur"

    cle2 = 40
    assert chiffre_cesar("abcdef", cle2) == "opqrst"

    cle3 = 0
    assert chiffre_cesar("test tést", cle3) == "test test"

    # Test unitaire déchiffrage
    txt1 = dechiffre_cesar("dcrur", cle1)
    assert txt1 == "azoro"

    txt2 = dechiffre_cesar("opqrst", cle2)
    assert txt2 == "abcdef"

    txt3 = dechiffre_cesar("test tést", cle3)
    assert txt3 == "test test"

    # Test unitaire chiffrage Vigenere
    assert chiffre_vigenere("Bonsoir", "hey") == "islzsgy"
    assert chiffre_vigenere("bonsoir bonsoir", "hey") == "islzsgy islzsgy"
    assert chiffre_vigenere("ééé", "a") == "eee"

    # Test unitaire déchiffrage Vigenere
    assert dechiffre_vigenere("islzsgy", "hey") == "bonsoir"
    assert dechiffre_vigenere("islzsgy islzsgy", "hey") == "bonsoir bonsoir"


print("ok")

if __name__ == "__main__":
    main()

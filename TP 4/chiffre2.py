#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 20 10:09:26 2019
@author: b19018426
"""

import string
import chaine


def chiffre_cesar(txt, cle):
    """
    Remplace chaque lettre d'une message par une 
    lettre à distance fixe dans l'ordre de l'aphabet.
    """
    if txt is None:
        return None

    if type(cle) != int:
        return None

    result = ""
    alphabet = string.ascii_lowercase

    for i in range(len(txt)):
        if txt[i] == " ":
            result += txt[i]
        pos = alphabet.find(txt[i])
        posi = (pos + cle) % 26
        result += alphabet[posi]
    return result


def dechiffre_cesar(txt, cle):
    """
    Décale dans l'alphabet pour obtenir le message de départ
    """
    if txt is None:
        return None

    if type(cle) != int:
        return None

    result = ""
    alphabet = string.ascii_lowercase

    for i in range(len(txt)):
        if txt[i] == " ":
            result += txt[i]
        pos = alphabet.find(txt[i])
        posi = (pos - cle) % 26
        result += alphabet[posi]
    return result


def main():
    # Test unitaire chiffrage
    cle1 = 3
    txt1 = chaine.nettoie("azoro")
    assert chiffre_cesar(txt1, cle1) == "dcrur"

    cle2 = 40
    txt2 = chaine.nettoie("abcdef")
    assert chiffre_cesar(txt2, cle2) == "opqrst"

    # Test unitaire déchiffrage
    txt3 = dechiffre_cesar("dcrur", cle1)
    assert txt3 == "azoro"

    txt4 = dechiffre_cesar("opqrst", cle2)
    assert txt4 == "abcdef"


print("ok")

if __name__ == "__main__":
    main()
